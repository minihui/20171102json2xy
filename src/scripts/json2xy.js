let initData;
let jsonArr = [];
let finalData = []; // 用户最终配置完整数据
let addBtn = document.querySelector("#addBtn"); //添加数据按钮
let dataGroup = document.querySelector("#dataGroup"); //可视化选项版块
let jsonData = document.querySelector("#jsonData"); //json数据显示框
let leftTitle = document.querySelector(".left-title");
let rightTitle = document.querySelector(".right-title");
let leftTitleI = leftTitle.querySelector("i"); //可视化选项错误提示
let rightTitleI = rightTitle.querySelector("i"); //数据框错误提示
let attrPart = document.querySelector(".attr"); //稿件属性
let attrCheckBox = attrPart.querySelectorAll("input");

// 默认数据设置
let defaultData = {
    "descript": undefined, // 生成注释
    "dataname": undefined, // 数据key值
    "informid": undefined, //信息片id
    "begin": 0, // 起始条数
    "end": 5, //结束条数
    "attr": "", // 属性
    "dayspan": 0, //稿件最多的上限总条数,"0"表示未设置
    "artattr": 0, //稿件类型： 0:全部;1:实体稿2:链接稿
    "keyword": "",
    "props": ["title", "url", "imgsrc", "abstract", "pretitle", "subtitle", "keyword", "pubtime", "author", "editor", "source"] // 属性列表
};

/* 当添加按钮点击添加数据 */
function addGroup() {
    let dataGroupInput = dataGroup.querySelectorAll("input"); //可视化选项版块所有的选择项
    let len = dataGroupInput.length;
    let jsonObj = {}; //json数据
    let propArr = [], //属性列表数组
        attrArr = []; //稿件属性数组
    /* 
     * 历遍所有的选择项，
     * 把对应的json的key和值组合成一个新的对象放在jsonObj这个对象中,
     * 这就是未来转化成翔宇的参数配置项
     */
    for (let index = 0; index < len; index++) {
        let val = dataGroupInput[index].value;
        let name = dataGroupInput[index].name;
        // console.log(val)
        switch (name) {
            case "Descript":
                // 注释，不能为空
                if (val == "") {
                    leftTitleI.innerHTML = "注释不能为空!";
                    setTimeout(() => {
                        leftTitleI.innerHTML = "";
                    }, 3000)
                    return;
                } else {
                    jsonObj[name.toLowerCase()] = val;
                }
                break;
            case "DataName":
                // ul的dataname，不能为空，不能重复
                if (val == "") {
                    leftTitleI.innerHTML = '翔宇ul的dataname，不能为空!';
                    setTimeout(() => {
                        leftTitleI.innerHTML = "";
                    }, 3000)
                    return;
                } else {
                    jsonObj[name.toLowerCase()] = val;
                }
                break;
            case "InformId":
                // 信息片id，不能为空
                if (val == "") {
                    leftTitleI.innerHTML = "信息片id不能为空!";
                    setTimeout(() => {
                        leftTitleI.innerHTML = "";
                    }, 3000)
                    return;
                } else {
                    jsonObj[name.toLowerCase()] = val;
                }
                break;
            case "begin":
                // 循环条数的起始稿件位置，不填写默认为0，从第一条开始
                if (val == "") {
                    jsonObj[name.toLowerCase()] = 0;
                } else {
                    jsonObj[name.toLowerCase()] = val;
                }
                break;
            case "end":
                //  循环条数的结束稿件位置，不填写默认为5
                if (val == "") {
                    jsonObj[name.toLowerCase()] = 5;
                } else {
                    jsonObj[name.toLowerCase()] = val;
                }

                if (val >= 100) {
                    jsonObj["dayspan"] = val;
                } else {
                    jsonObj["dayspan"] = "";
                }
                break;
            case "Attr":
                // 稿件属性，图片头条普通
                if (dataGroupInput[index].checked) {

                    attrArr.push(dataGroupInput[index].value)
                }
                jsonObj[name.toLowerCase()] = attrArr.join("");
                break;
                // case "Dayspan":
                //     // 稿件上限数目，不填写默认系统会取100
                //     jsonObj[name.toLowerCase()] = val;
                //     break;
            case "Artattr":
                // 稿件的类型，实体还是链接稿，默认是全部类型
                // console.log(val)
                if (dataGroupInput[index].checked) {
                    // console.log(val)
                    jsonObj[name.toLowerCase()] = val;
                }

                break;
            case "Keyword":
                // 关键词，用来筛选稿件，填写时，则只会显示有该关键字的稿件
                jsonObj[name.toLowerCase()] = val;
                break;
            case "props":
                // 选择稿件要吐出来的属性(标题，摘要，标题图等)
                if (dataGroupInput[index].checked) {
                    propArr.push(dataGroupInput[index].value)
                }
                jsonObj[name.toLowerCase()] = propArr;
                break;
        }
    }
    if (jsonData.value == "") {
        // 如果json数据框内没内容，则生成json格式的数据添加到框中
        jsonArr.push(jsonObj);
    } else {
        // 如果json数据框内已经有内容，则结合框内的数据生成新的json格式的数据添加到框中
        jsonArr = JSON.parse(jsonData.value);
        jsonArr.push(jsonObj)
    }
    // debugger

    // 将数据添加到json的textarea里显示
    jsonData.value = format(JSON.stringify(jsonArr), false);
    getJsonData(jsonData.value);
    // 添加完毕内容后，清空可视化选项
    for (let index = 0; index < len; index++) {
        let val = dataGroupInput[index].value;
        let name = dataGroupInput[index].name;
        // console.log(dataGroupInput[index])
        switch (name) {
            case "Attr":
                // console.log(dataGroupInput[index])
                if (dataGroupInput[index].value == "") {
                    dataGroupInput[index].checked = true;
                } else {
                    dataGroupInput[index].checked = false;
                }
                break;
            case "Artattr":
                // console.log(dataGroupInput[index].checked)
                if (dataGroupInput[index].value == 0) {
                    dataGroupInput[index].checked = true;
                } else {
                    dataGroupInput[index].checked = false;
                }
                break;
            case "props":
                // console.log(dataGroupInput[index].checked)
                if (dataGroupInput[index].value == "title" || dataGroupInput[index].value == "imgsrc" || dataGroupInput[index].value == "url") {
                    dataGroupInput[index].checked = true;
                } else {
                    dataGroupInput[index].checked = false;
                }
                break;
            case "begin":
                // console.log(dataGroupInput[index].checked)
                dataGroupInput[index].value = "0";
                break;
            case "end":
                // console.log(dataGroupInput[index].checked)
                dataGroupInput[index].value = "5";
                break;
            default:
                dataGroupInput[index].value = "";
                break;
        }
    }
}

function getJsonData(val) {
    let str = val;
    try {
        JSON.parse(str);
        // console.log("str是json字符串");
    } catch (e) {
        console.log(e);
        // alert("str不是json字符串");
    }
    initData = JSON.parse(str);
    json2xy(initData)
    return;

}
addBtn.addEventListener("click", addGroup);
/* 当json数据框内容被手动修改 */
function getJsonDataManual() {
    // let str = val;
    let str = document.querySelector("#jsonData").value;
    // console.log(str)
    if (str !== "") {
        try {
            JSON.parse(str);
            // console.log("str是json字符串");
        } catch (e) {
            console.log(e);
            // alert("str不是json字符串");
        }
        initData = JSON.parse(str);

        json2xy(initData)
    } else {
        document.querySelector("#xyData").value = "";
    }

    return;

}
jsonData.addEventListener("change", getJsonDataManual);

/* 稿件属性点击  如果选择了特殊属性  全属性勾选去掉 */
for (let i = 0; i < attrCheckBox.length; i++) {
    let attrInput = attrCheckBox[i]
    attrInput.addEventListener("click", attrCheck);
}

function attrCheck() {
    // console.log(this)

    if ((this.value == "+61" || this.value == "+62" || this.value == "+63") && this.checked) {
        // console.log(attrCheckBox[0])
        attrCheckBox[0].checked = false;
    } else {
        attrCheckBox[1].checked = false;
        attrCheckBox[2].checked = false;
        attrCheckBox[3].checked = false;
    }


}





/*
 * 转化方法：将翔宇数据配置对象编译成翔宇片段
 * @param {json} data 翔宇数据配置对象
 * @return {string} 相应的翔宇字符串片段
 */
function json2xy(data) {
    // console.log(data)
    // debugger;
    let xydata = '';
    let props = '';
    let descript = '',
        dataname = '',
        informid = '',
        begin = 0,
        end = 5,
        attr = '',
        keyword = '',
        dayspan = '0',
        artattr = '0';
    let title = '',
        imgsrc = '',
        url = '',
        abstract = '',
        pretitle = '',
        subtitle = '',
        propKeyword = '',
        pubtime = '',
        author = '',
        editor = '',
        source = '',
        seo = '&lt;span data-prop=#enpquot#seo#enpquot#&gt;&lt;Url&gt;&lt;a href=ArticleUrlPh &gt;&lt;/Url&gt;&lt;Title length=#enpquot#0#enpquot#&gt;TitlePh&lt;/Title&gt;&lt;/a&gt;&lt;/span&gt;\n\n';
    let element = {};
    let datanameArr = [];
    // 转化算法
    for (let i = 0; i < data.length; i++) {
        // let element = data[i];
        // 判断dataname是否重复
        datanameArr.push(data[i].dataname);
        datanameArr.sort();
        for (let j = 0; j < datanameArr.length; j++) {
            if (datanameArr[j] == datanameArr[j + 1]) {
                rightTitleI.innerHTML = "dataname不能重复!";
                setTimeout(() => {
                    rightTitleI.innerHTML = "";
                }, 3000)　
            }
        }
        // 以下是json转翔宇
        element = extendObj(defaultData, data[i]);
        // console.log(element)
        finalData.push(element)
        // console.log(finalData)
        for (let key in element) {
            // console.log(key+" : "+element[key])
            switch (key) {
                case "descript":
                    // 注释
                    descript = '<!-- ' + element[key] + ' -->\n\n';
                    break;
                case "dataname":
                    // 数据key值
                    dataname = '<ul data-name="' + element[key] + '">\n\n';
                    break;
                case "informid":
                    // 信息片id
                    informid = 'nodeid="' + element[key] + '"';
                    break;
                case "begin":
                    // 循环起始值
                    begin = 'Begin=' + element[key];
                    break;
                case "end":
                    // 循环结束值
                    end = 'End=' + element[key];
                    break;
                case "attr":
                    // 属性
                    attr = 'attr="' + element[key] + '"';
                    break;
                case "dayspan":
                    // 稿件上限值
                    dayspan = 'dayspan="' + element[key] + '"';
                    break;
                case "artattr":
                    // 稿件类型
                    artattr = 'artattr="' + element[key] + '"';
                    break;
                case "keyword":
                    // 关键字过滤稿子
                    keyword = 'keyword="' + element[key] + '"';
                    break;
                default:
                    // 翔宇吐出的参数值
                    props = (function (obj) {
                        let j = 0,
                            len = obj.length,
                            propList = "",
                            str = "";
                        for (; j < len; j++) {
                            propList = obj[j];
                            switch (propList) {
                                case "title":
                                    // 标题
                                    title = '&lt;span data-prop=#enpquot#title#enpquot#&gt;&lt;Title length=#enpquot#-1#enpquot#&gt;TitlePh&lt;/Title&gt;&lt;/span&gt;\n\n';
                                    break;
                                case "imgsrc":
                                    // 图片
                                    imgsrc = '&lt;span data-prop=#enpquot#imgsrc#enpquot#&gt;&lt;Picture needcode=0&gt;PictureUrlPh&lt;/Picture&gt;&lt;/span&gt;\n\n';
                                    break;
                                case "url":
                                    // 地址
                                    url = '&lt;span data-prop=#enpquot#url#enpquot#&gt;&lt;Url&gt;ArticleUrlPh&lt;/Url&gt;&lt;/span&gt;\n\n';
                                    break;
                                case "abstract":
                                    // 摘要
                                    abstract = '&lt;span data-prop=#enpquot#abstract#enpquot#&gt;&lt;Abstract&gt;AbstractPh&lt;/Abstract&gt;&lt;/span&gt;\n\n';
                                    break;
                                case "pretitle":
                                    // 引题
                                    pretitle = '&lt;span data-prop=#enpquot#pretitle#enpquot#&gt;&lt;PreTitle&gt;PreTitlePh&lt;/PreTitle&gt;&lt;/span&gt;\n\n';
                                    break;
                                case "subtitle":
                                    // 副题
                                    subtitle = '&lt;span data-prop=#enpquot#subtitle#enpquot#&gt;&lt;Subtitle&gt;SubtitlePh&lt;/Subtitle&gt;&lt;/span&gt;\n\n';
                                    break;
                                case "keyword":
                                    // 关键字
                                    propKeyword = '&lt;span data-prop=#enpquot#keyword#enpquot#&gt;&lt;Keyword&gt;KeywordPh&lt;/Keyword&gt;&lt;/span&gt;\n\n';
                                    break;
                                case "pubtime":
                                    // 发布时间
                                    pubtime = '&lt;span data-prop=#enpquot#pubtime#enpquot#&gt;&lt;PubTime Language=1 BriefMonth=0 BriefWeek=0&gt; YearPh-MonthPh-DayPh HourPh:MinutePh:SecondPh &lt;/PubTime&gt;&lt;/span&gt;\n\n';
                                    break;
                                case "author":
                                    // 作者
                                    author = '&lt;span data-prop=#enpquot#author#enpquot#&gt;&lt;Author&gt;AbstractPh&lt;/Author&gt;&lt;/span&gt;\n\n';
                                    break;
                                case "editor":
                                    // 编辑
                                    editor = '&lt;span data-prop=#enpquot#editor#enpquot#&gt;&lt;Editor&gt;EditorPh&lt;/Editor&gt;&lt;/span&gt;\n\n';
                                    break;
                                case "source":
                                    // 来源
                                    source = '&lt;span data-prop=#enpquot#source#enpquot#&gt;&lt;Source&gt;SourcePh&lt;/Source&gt;&lt;\n\n';
                                    break;
                            }
                            str += (title + imgsrc + url + abstract + pretitle + subtitle + propKeyword + pubtime + author + editor + source);
                            title = imgsrc = url = abstract = pretitle = subtitle = propKeyword = pubtime = author = editor = source = "";
                        } //for end
                        return str;
                    })(element[key])

            } //switch
        }

        xydata +=
            descript +
            dataname +
            '<!--webbot bot="AdvTitleList" ' + informid + ' type="0" spanmode="0" ' + dayspan + ' ' + attr + ' comstring="&lt;Repeat ' + begin + ' ' + end + '&gt;&lt;Article&gt;&lt;li&gt;\n\n' +
            seo +
            props +
            '&lt;/li&gt;&lt;/Article&gt;&lt;/Repeat&gt;" TAG="BODY" PREVIEW="[高级标题列表]" ' + artattr + ' isshowcode="0" titlekeyword="" ' + keyword + ' tagstring="00" starttime="" endtime="" id="0" startspan -->\n\n' +
            '<!--webbot bot="AdvTitleList" endspan i-checksum="0" -->\n\n' +
            '</ul>\n';

        // console.log(descript + name)
        // xydata += xydata
    }
    // console.log(xydata)
    document.getElementById("xyData").value = xydata
    return xydata;
}

function cloneObj(oldObj) { //复制对象方法
    if (typeof (oldObj) != 'object') return oldObj;
    if (oldObj == null) return oldObj;
    let newObj = new Object();
    for (let i in oldObj)
        newObj[i] = cloneObj(oldObj[i]);
    return newObj;
};

function extendObj() { //扩展对象
    let args = arguments;
    if (args.length < 2) return;
    let temp = cloneObj(args[0]); //调用复制对象方法
    for (let n = 1; n < args.length; n++) {
        for (let i in args[n]) {
            temp[i] = args[n][i];
        }
    }
    return temp;
}
// console.log(json2xy(initData));


/* tab选项卡动画 */
let tabOpts = document.querySelector(".tab-opts");
let tabCont = document.querySelector(".tab-cont");
let tabSpan = tabOpts.querySelectorAll("span");
let tabItem = tabCont.querySelectorAll(".item");
for (let i = 0; i < tabSpan.length; i++) {
    // tabSpan[i].setAttribute("class", "");
    tabSpan[i].index = i;
    tabItem[i].index = i;
    // tabItem[i].setAttribute("class", "item");
}
tabOpts.onclick = (ev) => {
    var ev = ev || window.event;
    var target = ev.target || ev.srcElement;

    if (target.nodeName.toLowerCase() == "span") {
        // console.log(target.index)
        for (let i = 0; i < tabSpan.length; i++) {
            if (i == target.index) {
                tabSpan[i].setAttribute("class", "active");
                tabItem[i].setAttribute("class", "item active");
            } else {
                tabSpan[i].setAttribute("class", "");
                tabItem[i].setAttribute("class", "item");
            }
        }
    }
}


/* json整理格式 */

function format(txt, compress /*是否为压缩模式 true为压缩，false为不压缩*/ ) { /* 格式化JSON源码(对象转换为JSON文本) */
    var indentChar = '    ';
    if (/^\s*$/.test(txt)) {
        console.log('数据为空,无法格式化! ');
        return;
    }
    try {
        var data = eval('(' + txt + ')');
    } catch (e) {
        console.log('数据源语法错误,格式化失败! 错误信息: ' + e.description, 'err');
        return;
    };
    var draw = [],
        last = false,
        This = this,
        line = compress ? '' : '\n',
        nodeCount = 0,
        maxDepth = 0;

    var notify = function (name, value, isLast, indent /*缩进*/ , formObj) {
        nodeCount++; /*节点计数*/
        for (var i = 0, tab = ''; i < indent; i++) tab += indentChar; /* 缩进HTML */
        tab = compress ? '' : tab; /*压缩模式忽略缩进*/
        maxDepth = ++indent; /*缩进递增并记录*/
        if (value && value.constructor == Array) { /*处理数组*/
            draw.push(tab + (formObj ? ('"' + name + '":') : '') + '[' + line); /*缩进'[' 然后换行*/
            for (var i = 0; i < value.length; i++)
                notify(i, value[i], i == value.length - 1, indent, false);
            draw.push(tab + ']' + (isLast ? line : (',' + line))); /*缩进']'换行,若非尾元素则添加逗号*/
        } else if (value && typeof value == 'object') { /*处理对象*/
            draw.push(tab + (formObj ? ('"' + name + '":') : '') + '{' + line); /*缩进'{' 然后换行*/
            var len = 0,
                i = 0;
            for (var key in value) len++;
            for (var key in value) notify(key, value[key], ++i == len, indent, true);
            draw.push(tab + '}' + (isLast ? line : (',' + line))); /*缩进'}'换行,若非尾元素则添加逗号*/
        } else {
            if (typeof value == 'string') value = '"' + value + '"';
            draw.push(tab + (formObj ? ('"' + name + '":') : '') + value + (isLast ? '' : ',') + line);
        };
    };
    var isLast = true,
        indent = 0;
    notify('', data, isLast, indent, false);
    return draw.join('');
}



/* ajax */

var Data2Html = function (options) {
    this.options = {
        nid: options.nid || 1183668,
        pgnum: options.pgnum || 1,
        cnt: options.cnt || 10,
        tp: options.tp || 1,
        orderby: options.orderby || 1,
        success: options.success || function (data) {
            console.log("bb")
        }
    };
    this.setRequestUrl();
}
Data2Html.prototype = {
    constructor: Data2Html,
    setRequestUrl: function () {
        // console.log(this.options)
        var opt = this.options;
        let callbackName = this.creatCallback();
        let url = 'http://qc.wa.news.cn/nodeart/list?nid=' + opt.nid + '&pgnum=' + opt.pgnum + '&cnt=' + opt.cnt + '&tp=' + opt.tp + '&orderby=' + opt.orderby + '?callback=' + callbackName + '& _ = ';
        this.addScirpt(url, callbackName);

    },
    creatCallback: function () {
        return "callback" + (Math.random() * 100000).toFixed(0)
    },
    addScirpt: function (src, callbackName) {
        var script = document.createElement("script");
        script.src = src;

        document.body.appendChild(script);
        script.parentNode.removeChild(script)
        let successName = this.creatCallback();
        window[successName] = this.callbackData.bind(this);
        eval("function " + callbackName + "(data){" + successName + "(data)}; window. " + callbackName + "=" + callbackName)
    },
    callbackData: function (data) {
        // this.opertate(data);
        this.options.success(data);

    },
    rebuild: function (options) {

    }

}


let testOpts = document.querySelector("#testOpts"); //测试数据选择显示框
let addBtn2 = testOpts.querySelector("#addBtn2"); //测试数据添加按钮
/* 添加测试数据 */

function addTestData() {
    let testOptsInput = testOpts.querySelectorAll("input"); //可视化选项版块所有的选择项
    var testParam = {}; //测试选项整合整成对象
    testOptsInput.forEach(element => {
        // console.log(element.value)
        testParam[element.name] = element.value
    });
    
    var opts = {
        nid: testParam.NodeId,
        // cnt: 5,
        success: function (data) {
            if (data.status == 0) {
                var list = data.data.list;
                var temp = [];
                var count = 0;
                function superposition(element) {
                    return `<li><span data-prop='title'>${element.Title}</span><span data-prop='imgsrc'>${element.imgsrc}</span><span data-prop='url'>${element.LinkUrl}</span><span data-prop='abstract'>${element.Abstract}</span><span data-prop='pretitle'>${element.IntroTitle}</span><span data-prop='subtitle'>${element.SubTitle}</span><span data-prop='keyword'>${element.keyword}</span><span data-prop='pubtime'>${element.PubTime}</span><span data-prop='author'>${element.Author}</span><span data-prop='editor'>${element.Editor}</span><span data-prop='source'>${element.SourceName}</span></li>\n\n`;
                }

                function checkAppend(element, index) {
                    
                    count++;
                    temp[index] = superposition(element);
                    if (count == 10) {
                        document.getElementById("testData").value = 
                            `<!-- ${testParam.testDescript} -->\n\n<ul data-name="${testParam.testDataName}">\n\n\t${temp.join("\t")}</ul>`
                    }
                }
                list.forEach((element, index) => {
                    var imgsrc = "";
                    if (element.allPics !== null) {
                        if (testParam.imgWidth !== "" && testParam.imgHeight !== "") {
                            var picSize = {
                                // imgurl: "images/1121300560_1500517367661_title0h.png",
                                imgurl: element.allPics[0],
                                width: testParam.imgWidth,
                                height: testParam.imgHeight
                            }
                            drawBase64Image(picSize, function (imgUrl) {
                                element.imgsrc = imgUrl;
                                checkAppend(element, index);
                             })
                            /* getBase64Image(picSize, function (base64Url) {
                                element.imgsrc = base64Url;
                                checkAppend(element, index);
                            }); */

                        } else {
                            element.imgsrc = element.allPics[0];
                            checkAppend(element, index);
                        }
                    } else {
                        element.imgsrc = null;
                        checkAppend(element, index);
                    }

                });

            }
        }
    }
    new Data2Html(opts);
}
/* 添加测试数据按钮点击 */
addBtn2.addEventListener("click", addTestData);


/* 绘制base64色块 */

/* function getBase64Image(options, callback) {
    let img = new Image();
    // img.setAttribute('crossOrigin', 'anonymous');
    let canvas = document.createElement("canvas");
    let ctx = canvas.getContext("2d");
    img.onload = function () {
        canvas.width = options.width;
        canvas.height = options.height;
        // document.body.appendChild(img);
        // document.body.appendChild(canvas);
        ctx.drawImage(img, 0, 0, options.width, options.height);

        var ext = img.src.substring(img.src.lastIndexOf(".") + 1).toLowerCase();
        var dataURL = canvas.toDataURL("image/" + ext);
        callback(dataURL);

    }
    img.src = options.imgurl;

} */
function drawBase64Image(options,callback) {
    // let img = new Image();
    let canvas = document.createElement("canvas");
    let ctx = canvas.getContext("2d");
    canvas.width = options.width;
    canvas.height = options.height;
    ctx.fillStyle = "#666";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.strokeStyle = "#fff";
    ctx.font = "24px Arial";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.strokeText(canvas.width + "*" + canvas.height, canvas.width / 2, canvas.height/2);
    
    var dataURL = canvas.toDataURL();
    var img = new Image();
    img.src = dataURL;
    img.classList.add("mystyle");
    document.body.appendChild(img)
    callback(dataURL);
}


/* 测试和正式数据切换 */

/* tab选项卡动画 */
let switchBtns = document.querySelector(".switchBtns");
let switchConts = document.querySelector(".switchConts");
let switchSpan = switchBtns.querySelectorAll("span");
let switchItem = switchConts.querySelectorAll(".box");
for (let i = 0; i < tabSpan.length; i++) {
    // tabSpan[i].setAttribute("class", "");
    switchSpan[i].index = i;
    switchItem[i].index = i;
    // tabItem[i].setAttribute("class", "item");
}
switchBtns.onclick = (ev) => {
    var ev = ev || window.event;
    var target = ev.target || ev.srcElement;

    if (target.nodeName.toLowerCase() == "span") {
        // console.log(target.index)
        for (let i = 0; i < tabSpan.length; i++) {
            if (i == target.index) {
                switchSpan[i].classList.add("on");
                switchItem[i].classList.add("on");
            } else {
                switchSpan[i].classList.remove("on");
                switchItem[i].classList.remove("on");
            }
        }
    }
}