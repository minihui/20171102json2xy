// 使用文件生成翔宇加测试页模块
(function () {
    let filebuilder = document.querySelector('.filebuilder'),
        tplForTest = document.querySelector('#xtplFileTest'),
        tplForOnline = document.querySelector('#xtplFileOnline'),
        onlineRst = document.querySelector('#online-build-rst'),
        testRst = document.querySelector('#test-build-rst'),
        switchBtn = document.querySelector('#loadfile-switch'),
        errmsg = document.querySelector('#errmsg');

    function buildFile(file) {
        // 正式页面
        pageBuilder.buildOnline(file,function(err, file){
            if(err){
                onlineRst.value = err;
            } else {
                onlineRst.value = file;
            }
        });

        // 测试页面
        pageBuilder.buildTest(file,function(err, file){
            if(err){
                testRst.value = err;
            } else {
                testRst.value = file;
            }
        });
    }

    // 面板开关
    switchBtn.addEventListener('click', function(e){
        filebuilder.classList.toggle('active');
    });

    // 监听并生成文件
    tplForOnline.addEventListener('change', function (e) {
        // 清空列表
        onlineRst.value = '页面生成中...'; 
        testRst.value = '页面生成中...';
        // 显示pannel
        filebuilder.classList.toggle('active',true);
        var fileRaeder = new FileReader();
        fileRaeder.readAsText(this.files[0], 'uft-8');

        fileRaeder.onload = function (e) {
            var file = e.target.result;

            buildFile(file); 
        }; 
    });
    
})();