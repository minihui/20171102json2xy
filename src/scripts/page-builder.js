var pageBuilder = (function () {
    var err = null;
    /**
     * 生成翔宇数据源片段
     * 
     * @param {any} data 数据配置 
     * @returns 翔宇数据源，放置在 #xData 中
     */
    function json2xy(data) {
        // 默认数据设置
        let defaultData = {
                "descript": undefined, // 生成注释
                "dataname": undefined, // 数据key值
                "informid": undefined, //信息片id
                "begin": 0, // 起始条数
                "end": 5, //结束条数
                "attr": "", // 属性
                "dayspan": 0, //稿件最多的上限总条数,"0"表示未设置
                "artattr": 0, //稿件类型： 0:全部;1:实体稿2:链接稿
                "keyword": "",
                "props": ["title", "url", "imgsrc", "abstract", "pretitle", "subtitle", "keyword", "pubtime", "author", "editor", "source"] // 属性列表
            },
            xytpl = '',
            props = '',
            descript = '',
            dataname = '',
            informid = '',
            begin = 0,
            end = 5,
            attr = '',
            keyword = '',
            dayspan = '0',
            artattr = '0',
            title = '',
            imgsrc = '',
            url = '',
            abstract = '',
            pretitle = '',
            subtitle = '',
            propKeyword = '',
            pubtime = '',
            author = '',
            editor = '',
            source = '',
            seo = '&lt;span data-prop=#enpquot#seo#enpquot#&gt;&lt;Url&gt;&lt;a href=ArticleUrlPh &gt;&lt;/Url&gt;&lt;Title length=#enpquot#0#enpquot#&gt;TitlePh&lt;/Title&gt;&lt;/a&gt;&lt;/span&gt;\n',
            element = {},
            datanameArr = [],
            finalData = []; // 用户最终配置完整数据

        function cloneObj(oldObj) { //复制对象方法
            if (typeof (oldObj) != 'object') {
                return oldObj;
            }

            if (oldObj == null) {
                return oldObj;
            }

            let newObj = new Object();

            for (let i in oldObj) {
                newObj[i] = cloneObj(oldObj[i]);
            }

            return newObj;
        }

        function extendObj() { //扩展对象
            let args = arguments;
            if (args.length < 2) return;
            let temp = cloneObj(args[0]); //调用复制对象方法
            for (let n = 1; n < args.length; n++) {
                for (let i in args[n]) {
                    temp[i] = args[n][i];
                }
            }
            return temp;
        }

        // 转化算法
        for (let i = 0; i < data.length; i++) {
            datanameArr.push(data[i].dataname);
            datanameArr.sort();
            for (let j = 0; j < datanameArr.length; j++) {
                if (datanameArr[j] == datanameArr[j + 1]) {
                    console.log('数据名称重复: ' + datanameArr[j]);
                }
            }

            element = extendObj(defaultData, data[i]);
            finalData.push(element);

            // 生成翔宇属性
            function makeTag(name, content) {
                return '&lt;span data-prop=#enpquot#{{name}}#enpquot#&gt;{{content}}&lt;/span&gt;\n'
                    .replace('{{name}}', name)
                    .replace('{{content}}', content);
            }

            for (let key in element) {
                // console.log(key+" : "+element[key])
                switch (key) {
                    case "descript":
                        // 注释
                        descript = '<!-- ' + element[key] + ' -->\n';
                        break;
                    case "dataname":
                        // 数据key值
                        dataname = '<ul data-name="' + element[key] + '">\n';
                        break;
                    case "informid":
                        // 信息片id
                        informid = 'nodeid="' + element[key] + '"';
                        break;
                    case "begin":
                        // 循环起始值
                        begin = 'Begin=' + element[key];
                        break;
                    case "end":
                        // 循环结束值
                        end = 'End=' + element[key];
                        break;
                    case "attr":
                        // 属性
                        attr = 'attr="' + element[key] + '"';
                        break;
                    case "dayspan":
                        // 稿件上限值
                        dayspan = 'dayspan="' + element[key] + '"';
                        break;
                    case "artattr":
                        // 稿件类型
                        artattr = 'artattr="' + element[key] + '"';
                        break;
                    case "keyword":
                        // 关键字过滤稿子
                        keyword = 'keyword="' + element[key] + '"';
                        break;
                    case "props":
                        // 翔宇吐出的参数值
                        props = (function (obj) {
                            let j = 0,
                                len = obj.length,
                                propList = "",
                                str = "";
                            for (; j < len; j++) {
                                propList = obj[j];
                                switch (propList) {
                                    case "title":
                                        // 标题
                                        title = makeTag('title', '&lt;Title length=#enpquot#-1#enpquot#&gt;TitlePh&lt;/Title&gt;');
                                        break;
                                    case "imgsrc":
                                        // 图片
                                        imgsrc = makeTag('imgsrc', '&lt;Picture needcode=0&gt;PictureUrlPh&lt;/Picture&gt;');
                                        break;
                                    case "url":
                                        // 地址
                                        url = makeTag('url', '&lt;Url&gt;ArticleUrlPh&lt;/Url&gt;');
                                        break;
                                    case "abstract":
                                        // 摘要
                                        abstract = makeTag('abstract', '&lt;Abstract&gt;AbstractPh&lt;/Abstract&gt;');
                                        break;
                                    case "pretitle":
                                        // 引题
                                        pretitle = makeTag('pretitle', '&lt;PreTitle&gt;PreTitlePh&lt;/PreTitle&gt;');
                                        break;
                                    case "subtitle":
                                        // 副题
                                        subtitle = makeTag('subtitle', '&lt;Subtitle&gt;SubtitlePh&lt;/Subtitle&gt;');
                                        break;
                                    case "keyword":
                                        // 关键字
                                        propKeyword = makeTag('keyword', '&lt;Keyword&gt;KeywordPh&lt;/Keyword&gt;');
                                        break;
                                    case "pubtime":
                                        // 发布时间
                                        pubtime = makeTag('pubtime', '&lt;PubTime Language=1 BriefMonth=0 BriefWeek=0&gt; YearPh-MonthPh-DayPh HourPh:MinutePh:SecondPh &lt;/PubTime&gt;');
                                        break;
                                    case "author":
                                        // 作者
                                        author = makeTag('author', '&lt;Author&gt;AuthorPh&lt;/Author&gt;');
                                        break;
                                    case "editor":
                                        // 编辑
                                        editor = makeTag('editor', '&lt;Editor&gt;EditorPh&lt;/Editor&gt;');
                                        break;
                                    case "source":
                                        // 来源
                                        source = makeTag('source', '&lt;Source&gt;SourcePh&lt;/Source&gt;');
                                        break;
                                }
                                str += (title + imgsrc + url + abstract + pretitle + subtitle + propKeyword + pubtime + author + editor + source);
                                title = imgsrc = url = abstract = pretitle = subtitle = propKeyword = pubtime = author = editor = source = "";
                            } //for end
                            return str;
                        })(element[key])

                } //switch
            }

            xytpl +=
                descript +
                dataname +
                '<!--webbot bot="AdvTitleList" ' + informid + ' type="0" spanmode="0" ' + dayspan + ' ' + attr + ' comstring="&lt;Repeat ' + begin + ' ' + end + '&gt;&lt;Article&gt;&lt;li&gt;\n' +
                seo +
                props +
                '&lt;/li&gt;&lt;/Article&gt;&lt;/Repeat&gt;" TAG="BODY" PREVIEW="[高级标题列表]" ' + artattr + ' isshowcode="0" titlekeyword="" ' + keyword + ' tagstring="00" starttime="" endtime="" id="0" startspan -->\n' +
                '<!--webbot bot="AdvTitleList" endspan i-checksum="0" -->\n' +
                '</ul>\n';
        }

        return xytpl;
    }

    /**
     * 生成翔宇模板
     * 
     * @param {any} xdata 翔宇数据 
     * @param {any} html 原页面
     * @returns 翔宇模板
     */
    function buildPage(xdata, html) {
        var htm = html.replace(/(\s*)<div\s*?id="xData"[^>]*?>[\s\S]*?<\/div>/gm, function (all, $1) {
            return $1 + '<div id="xData" style="display:none;" data-build-time="' + ((new Date()).toLocaleString()) + '"' + '>\n' +
                xdata.replace(/^/gm, $1) + $1 + '</div>';
        });

        return htm;
    }

    /**
     * 将满足条件的随心模板处理为翔宇模板
     * @param {String} tpl 翔宇模板
     * @returns 数据配置原始值
     */
    function getDataFromScript(tpl) {
        var scriptTag = /<script (.+?)>([^]*?)<\/script>/gm,
            script,
            scripts = [],
            isXtpl = false,
            tpl2json = [];

        // 获取script列表
        while (script = scriptTag.exec(tpl)) {
            scripts.push(script[1]);
            scripts[scripts.length - 1] += (` data-content="${script[2]}"`);
        }

        scripts.forEach(function (item, index) {
            var props = /([\S]*)\s*="([^]*?)"/gm; // 匹配属性,如 a="a"
            var prop;

            if (item.indexOf('xtpl') > 0 && item.indexOf('auto') > 0) {
                isXtpl = true;
            }

            if (item.indexOf('text/xtpl') > 0) {
                tpl2json.push({});

                while (prop = props.exec(item)) {
                    tpl2json[tpl2json.length - 1][prop[1]] = prop[2];
                }
            }
        });

        if (isXtpl) {
            return tpl2json;
        } else {
            err = '错误：没有为 <script src="*.xtpl.js"></script> 添加 auto 属性';
            return false;
        }
    }

    /**
     * 将从script中获取的原始值转化为json配置
     * 
     * @param {any} data 原始值
     * @returns 用于json2xy的配置
     */
    function format2json(data) {
        "use strict";
        var allProps = ["title", "url", "imgsrc", "abstract", "pretitle", "subtitle", "keyword", "pubtime", "author", "editor", "source"];
        var propPatten = /{{(.*?)}}/gm;
        var formatJson = data.map(function (item) {
            var newItem = {
                "jid": "warn: set data-jid",
                "descript": "",
                "dataname": "warn: set data-name",
                "informid": "warn: set data-id",
                "attr": "",
                "props": [],
                "begin": "0",
                "end": "5",
                "dayspan": "",
                "artattr": "0",
                "keyword": ""
            };

            for (var prop in item) {
                if (item.hasOwnProperty(prop)) {
                    var propVal = item[prop].trim();

                    switch (prop) {
                        case 'data-name':
                            newItem.dataname = propVal;
                            break;
                        case 'data-id':
                            newItem.informid = Number(propVal);
                            break;
                        case 'data-jid':
                            newItem.jid = Number(propVal);
                            break;
                        case 'data-attrs':
                            if (propVal.length !== 0) {
                                var attrs = propVal.split(/\s+/g);
                                newItem.attr = attrs.length > 0 ? ('+' + attrs.join('+')) : '';
                            }
                            break;
                        case 'data-x-count':
                            newItem.end =
                                Number(propVal || 5) +
                                Number(item['data-x-skip'] ? item['data-x-skip'] : 0);
                            break;
                        case 'data-x-skip':
                            newItem.begin = Number(propVal) || 0;
                            break;
                        case 'data-content':
                            var propName = {};
                            var tplFrag = '';

                            while (tplFrag = propPatten.exec(propVal)) {
                                allProps.forEach(function (ele, idx) {
                                    if (tplFrag[1].indexOf(allProps[idx]) > -1) {
                                        if (!propName[allProps[idx]]) {
                                            propName[allProps[idx]] = true;
                                            newItem.props.push(allProps[idx]);
                                        }
                                    }
                                });
                            }

                            break;
                        default:
                            break;
                    }
                }

            }

            newItem.descript = item['data-name'] + '[' + item['data-id'] + ']';
            return newItem;
        });

        return formatJson;
    }

    // 由json生成测试数据
    function json2test(json, callback) {
        var xData = [];
        var loadCount = 0;

        // index: 序号, id: jid, attr: first attr, count: end-begin, props: TODO, name: data-name
        function loadData(index, id, attr, count, name, props) {
            var fn = 'callback_' + Date.now() + (Math.random()*100000).toFixed(0);
            console.log(fn);

            window[fn] = function (data) {

                xData[index] = renderTextUl(data.data.list, name, props);

                if (++loadCount === json.length) {
                    callback(null, xData.join('\n'));
                }
            };

            var url = `http://da.wa.news.cn/nodeart/page?nid=11145727&pgnum=1&cnt=${count}&attr=${attr}&tp=1&orderby=0&callback=${fn}`;

            var script = document.createElement('script');
            document.body.appendChild(script);
            script.src = url;
        }

        for (var i = 0; i < json.length; i++) {
            loadData(i, json[i].jid, json[i].attr.slice(1,3), json[i].end - json[i].begin, json[i].dataname,json[i].props);
        }
    }

    function renderTextUl(data, name, props) {
        var html = `<ul data-name="${name}">\n`;
        var itemtpl = `    <span data-prop='{{propName}}'>{{propVal}}</span>\n`;

        var renderMap = {
            'title': function (data) {
                return data.Title || '';
            },
            'url': function (data) {
                return data.LinkUrl || '';
            },
            'imgsrc': function (data) {
                return (data.allPics && data.allPics[0]) || '';
            },
            'abstract': function (data) {
                return data.Abstract || '';
            },
            'pretitle': function (data) {
                return data.IntroTitle || '';
            },
            'subtitle': function (data) {
                return data.SubTitle || '';
            },
            'keyword': function (data) {
                return data.keyword || '';
            },
            'pubtime': function (data) {
                return data.PubTime;
            },
            'author': function (data) {
                return data.Author || '';
            },
            'editor': function (data) {
                return data.Editor || '';
            },
            'source': function (data) {
                return data.SourceName || '';
            }
        };
        
        for (var i = 0; i < data.length; i++) {
            html += '<li>\n';

            for (var j in renderMap) {
                if(props.indexOf(j) > -1){
                    html +=
                    itemtpl
                    .replace('{{propName}}', j)
                    .replace('{{propVal}}', renderMap[j].call(null, data[i]));
                }
            }

            html += '</li>\n';
        }

        html += '</ul>\n';

        return html;
    }

    return {
        buildOnline: function (html, callback) {
            var scriptData = getDataFromScript(html);

            if (scriptData) {
                // 将配置格式转化成json格式 
                var jsonData = format2json(scriptData);
                // 生成翔宇
                var xData = json2xy(jsonData);
                // 渲染页面
                var finalPage = buildPage(xData, html);

                
            }
            callback(err, finalPage);
        },
        buildTest: function (html, callback) {
            var scriptData = getDataFromScript(html);

            if (scriptData) {
                // 将配置格式转化成json格式 
                var jsonData = format2json(scriptData);
                // 生成翔宇
                json2test(jsonData, function (err, xData) {
                    // 渲染页面
                    var finalPage = buildPage(xData, html);
                    callback(null, finalPage);
                });
            }
            if (err){
                callback(err, "");
            }
            
        }
    };
})();