var initData = [];
// 生成表格
class ViewConfigure extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.handleChange = this.handleChange.bind(this);
        // this.handleSubmit = this.handleSubmit.bind(this);
    }
    renerConfig(classStyle,labelName, typeName, actionName, placeholder) {
        return <div className={classStyle} ><label>{labelName}</label><input type={typeName} name={actionName} placeholder={placeholder}/> </div>
    }
    handleClick(e) {
        e.preventDefault();
        
    }
    

    handleChange(event) {
        // this.setState({ value: event.target.value });
        console.log(event.target)
        var jsonData={};
        switch (event.target.name) {
            case "Descript":
                jsonData = '"descript":' + event.target.value
                break;
            default:
                break;
        }
    }
    render() {
        return (
            <div className="main" value={this.state.value} onChange={this.handleChange}>
                <div className="dataGroup">
                    {
                        this.renerConfig("descript", "注释：", "text","Descript","必填，填写此段翔宇代码的说明")
                    }
                    {
                        this.renerConfig("dataKey", "dataKey：", "text", "DataKey", "必填，填写此段翔宇代码的ul的属性名")
                    }
                    {
                        this.renerConfig("nodeid", "信息片id：", "text", "Nodeid", "必填，填写此段翔宇代码的信息片id")
                    }
                    <div className="loop">
                        <span>循环条数：</span><br />
                        <label>begin：</label>
                            <input type="text" name="begin" placeholder="必填，begin" />
                        <label>end：</label>
                            <input type="text" name="end" placeholder="必填，end" />
                    </div>
                    {
                        this.renerConfig("attr", "属性：", "text", "Attr", "选填，属性：图片 +61,头条 +62,普通 +63")
                    }
                    {
                        this.renerConfig("dayspan", "稿件上限数量：", "text", "Dayspan", "选填，稿件最多的上限总条数,默认是100条")
                    }
                    {
                        this.renerConfig("artattr", "稿件类型：", "text", "Artattr", "选填，稿件类型：0:全部,1:实体稿,2:链接稿")
                    }
                    {
                        this.renerConfig("keyword", "关键词：", "text", "Keyword", "选填，用来筛选稿件")
                    }
                    <div className="props">
                        <span>属性列表：</span><br/>
                            <label>标题：</label>
                        <input name="props" type="checkbox" value="title" />
                            <label>链接：</label>
                        <input name="props" type="checkbox" value="url" /> 
                            <label>标题图：</label>
                        <input name="props" type="checkbox" value="imgsrc" /> 
                            <label>摘要：</label>
                        <input name="props" type="checkbox" value="abstract" /> 
                            <label>引题：</label>
                        <input name="props" type="checkbox" value="pretitle" /> 
                            <label>副题：</label>
                        <input name="props" type="checkbox" value="subtitle" />
                            <label>关键词：</label>
                        <input name="props" type="checkbox" value="keyword" /> 
                            <label>发布时间：</label>
                        <input name="props" type="checkbox" value="pubtime" /> 
                            <label>作者：</label>
                        <input name="props" type="checkbox" value="author" /> 
                            <label>编辑：</label>
                        <input name="props" type="checkbox" value="editor" /> 
                            <label>来源：</label>
                        <input name="props" type="checkbox" value="source" /> 
                    </div>
                </div>
                <button className="addData" onClick={(e) => this.handleClick(e)}>添加数据</button>
            </div>
        );
    }
}

ReactDOM.render(<ViewConfigure />, document.getElementById('viewFrame'))

class JsonConfigure extends React.Component{
    constructor(props) {
        super(props);
        this.state = { value: '' };

        this.handleChange = this.handleChange.bind(this);
        // this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
        console.log(typeof event.target.value)
        
            // initData.push(event.target.value)
            // console.log(initData)
        
    }

    // handleSubmit(event) {
    //     alert('A name was submitted: ' + this.state.value);
    //     event.preventDefault();
    // }
    render() {
        return (
            <textarea rows="30" cols="100" type="text" value={this.state.value} onChange={this.handleChange}>
                
            </textarea>
        )
    }
}
ReactDOM.render(<JsonConfigure />, document.getElementById('jsonFrame'))


